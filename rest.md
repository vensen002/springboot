# REST 风格



## REST 简介

- REST （Repersentational State Transfer），表现形式状态转换
  - 传统风格资源描述形式
    - http://localhost/user/getById?id=1
    - http://localhost/user/saveUser
  - REST风格描述形式
    - http://localhsot/user/1
    - http://localhsot/user
- 优点
  - 隐藏资源的访问行为，无法通过地址得知对资源是何种操作
  - 书写简化
- 按照REST风格访问资源时使用<font color=red>行为动作</font>区分对资源进行了何种操作
  - http://localhsot/users			查询全部用户信息		GET (查询)
  - http://localhsot/users/1		 查询指定用户信息		GET (查询)
  - http://localhsot/users			 添加用户信息				POST(新增/保存)
  - http://localhsot/users			 修改用户信息				PUT (修改/更新)
  - http://localhsot/users/1		  删除用户信息				DELETE (删除)

> <font color=red>注意事项</font>
>
> ​	上述行为是约定方式，约定不是规范，可以打破，所以称REST风格，而不是REST规范
>
> ​	描述模块的名称通常使用复数，也就是加s的风格描述，表示此类资源，而非单个资源
>
> ​	例如：users、books、accounts..

## RESTful 入门案例

- 名称：@RequestMapping

- 类型：<font color=red>方法注解</font>

- 位置：SpringMVC控制器方法定义上方

- 作用：设置当前控制器方法请求访问路径

- 范例：

  ```java
  @RequestMapping(value="/users", method=RequestMethod.POST)
  @ResponseBody
  public String save(@RequestBody User user) {
      System.out.println("user save..." + user);
      return "{'module' : 'user save'}";
  }
  ```

- 属性：

  - value：请求访问路径
  - method：http请求动作，标准动作（GET/POST/PUT/DELETE）



- 名称：@PathVariable

- 类型：<font color=red>形参注解</font>

- 位置：SpringMVC控制器方法形参定义前面

- 作用：绑定路径参数预处理器方法形参间的关系，要求路径参数名与形参名一一对应

- 范例：

  ```java
  @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
  @ResponseBody
  public String delete(@PathVariable Integer id) {
      System.out.println("user delete..." + id);
      return "{'module' : 'user delete'}";
  }
  ```

  

**@RequestBody** **@RequestParam** **@PathVariable** 比较

- 区别
  - @RequestParam 用于接受url地址传参或表单传参
  - @RequestBody 用于就收json数据
  - @PathVariable 用于接受路径参数，使用 {参数名称} 描述路径参数
- 应用
  - 后期开发中，发送请求参数超过1个时，以json格式为主，@Request Body应用较广
  - 如果发送非json格式参数，先用@RequsetParam接受请求参数
  - 采用RESTful进行开发，当参数数量较少时，比如1个，可以采用@Pathvariable接受请求路径变量，通常用于传递id值

## REST 快速开发

- 名称：@RsetController

- 类型：<font color=red>类注解</font>

- 位置：基于SpringMVC的RESTful开发控制器类定义上方

- 作用：设置当前控制器类为RESTful风格，等同于@Controller 与 @ResponseBody两个注解组合功能

- 范例：

  ```java
  @RsetController
  public class BookController {
  }
  ```



- 名称： @GetMapping 	@PostMapping 	@PutMapping 	@DeleteMapping

- 类型：<font color=red>方法注解</font>

- 位置：基于SpringMVC的RESTful开发控制器方法定义上方

- 作用：设置当前控制器方法请求访问路径与请求动作，每种对应一个请求动作

- 范例：

  ```java
  @GetMapping("/{id}")
  public String getById(@PathVariable Integer id) {
      System.out.println("book getById..." + id);
  	return "{'module' : 'book getById'}";
  }
  ```

- 属性

  - value：请求访问路径



## 案例：基于RESTful页面数据交互

