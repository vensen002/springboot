package com.vensen.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.PreparedStatement;

/**
 * @author by vense
 * @Classname BookController
 * @Description 测试
 * @Date 2022/4/13 10:41
 */
// Rest 模式
@RestController
@RequestMapping("/books")
public class BookController {

    @GetMapping
    public String getById() {
        System.out.println("springboot is running...");
        return "springboot is running... 02";
    }
}
