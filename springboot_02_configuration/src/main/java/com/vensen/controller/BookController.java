package com.vensen.controller;

import com.vensen.MyDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.PreparedStatement;

/**
 * @author by vense
 * @Classname BookController
 * @Description 测试
 * @Date 2022/4/13 10:41
 */
// Rest 模式
@RestController
@RequestMapping("/books")
public class BookController {

    // 读取yaml数据中的单一数据
    @Value("${country}")
    private String country1;

    @Value("${user.username}")
    private String name;

    @Value("${likes[1]}")
    private String like1;

    @Value("${tempDir}")
    private String tempDir;

    // 使用自动注入可以把所有yaml数据封装到Environment对象中
    @Autowired
    private Environment env;

    @Autowired
    private MyDataSource dataSource;

    @GetMapping
    public String getById() {
        System.out.println("springboot is running...");
        System.out.println("country1 ====>" + country1);
        System.out.println("name ====>" + name);
        System.out.println("like1 ====>" + like1);
        System.out.println("tempDir ====>" + tempDir);
        System.out.println("-------------------------");
        System.out.println(env.getProperty("country"));
        System.out.println(env.getProperty("user.username"));
        System.out.println("-------------------------");
        System.out.println(dataSource);
        return "springboot is running...";
    }
}
