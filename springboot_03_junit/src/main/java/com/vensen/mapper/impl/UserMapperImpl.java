package com.vensen.mapper.impl;

import com.vensen.mapper.UserMapper;
import org.springframework.stereotype.Repository;

/**
 * @author by vense
 * @Classname UserMapperImpl
 * @Description TODO()
 * @Date 2022/4/13 17:46
 */
@Repository
public class UserMapperImpl implements UserMapper {
    @Override
    public void save() {
        System.out.println("UserMapperImpl.save");
    }
}
