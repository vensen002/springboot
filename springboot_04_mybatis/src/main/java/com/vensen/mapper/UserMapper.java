package com.vensen.mapper;

import com.vensen.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author by vense
 * @Classname UserMapper
 * @Description TODO()
 * @Date 2022/4/13 18:14
 */
@Mapper
public interface UserMapper {

    @Select("select * from USER where id = #{id}")
    User getById(Long id);
}
