package com.vensen.model;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author by vense
 * @Classname User
 * @Description TODO()
 * @Date 2022/4/13 18:12
 */
@Data
public class User {

    private Long id;
    private String name;
    private Integer age;
    private String email;

}
