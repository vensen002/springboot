package com.vensen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vensen.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author by vense
 * @Classname UserMapper
 * @Description TODO()
 * @Date 2022/4/13 18:14
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
