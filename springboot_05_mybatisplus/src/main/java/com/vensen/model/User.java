package com.vensen.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author by vense
 * @Classname User
 * @Description TODO()
 * @Date 2022/4/13 18:12
 */
@Data
public class User {

    private Long id;
    private String name;
    private Integer age;
    private String email;

}
