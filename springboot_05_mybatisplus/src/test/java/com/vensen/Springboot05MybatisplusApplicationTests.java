package com.vensen;

import com.vensen.mapper.UserMapper;
import com.vensen.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Springboot05MybatisplusApplicationTests {

    @Autowired
    private UserMapper userMapper;


    @Test
    void contextLoads() {
        User user = userMapper.selectById(3L);
        System.out.println(user);
    }

    @Test
    void testAll() {
        List<User> list = userMapper.selectList(null);
        System.out.println(list);
    }

}
