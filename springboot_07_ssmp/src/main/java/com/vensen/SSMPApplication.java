package com.vensen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class SSMPApplication {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        // 在启动boot时断开读取外部临时配置
        SpringApplication.run(SSMPApplication.class);
    }

}
