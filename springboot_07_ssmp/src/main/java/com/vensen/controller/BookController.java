package com.vensen.controller;

import com.vensen.controller.utils.R;
import com.vensen.model.Book;
import com.vensen.service.BookService;
import com.vensen.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.jws.Oneway;
import java.io.IOException;
import java.util.List;

/**
 * @author by vense
 * @Classname BookController
 * @Description TODO()
 * @Date 2022/4/14 11:49
 */
@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private IBookService service;

    @GetMapping
    public R getAll() {
        return new R(true, service.list());
    }

    @PostMapping
    public R save(@RequestBody Book book) throws IOException {
        return new R(service.save(book));
    }

    @PutMapping
    public R update(@RequestBody Book book) {
        System.out.println(book);
        return new R(service.updateById(book));
    }

    @DeleteMapping("/{id}")
    public R delete(@PathVariable Integer id) {
        return new R(service.removeById(id));
    }

    @GetMapping("/{id}")
    public R getById(@PathVariable Integer id) {
        return new R(true, service.getById(id));
    }

    @GetMapping("/{currentPage}/{pageSize}")
    public R getPage(@PathVariable Integer currentPage,@PathVariable Integer pageSize, Book book) {
        System.out.println(book);
        return new R(true, service.getPage(currentPage, pageSize, book));
    }

}
