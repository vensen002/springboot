package com.vensen.controller.utils;

import lombok.Data;

/**
 * @author by vense
 * @Classname R
 * @Description TODO()
 * @Date 2022/4/14 15:15
 */
@Data
public class R {

    private Boolean flag;
    private Object data;
    private String msg;

    public R() {}

    public R(Boolean flag) {
        this.flag = flag;
    }

    public R(String msg) {
        this.flag = false;
        this.msg = msg;
    }

    public R(Boolean flag, Object data) {
        this.flag = flag;
        this.data = data;
    }

    public R(Boolean flag, Object data, String msg) {
        this.flag = flag;
        this.data = data;
        this.msg = msg;
    }
}
