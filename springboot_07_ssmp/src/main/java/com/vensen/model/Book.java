package com.vensen.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author by vense
 * @Classname Book
 * @Description TODO()
 * @Date 2022/4/13 19:24
 */
@Data
@TableName(value = "tbl_book")
public class Book {

//    @TableId(type = IdType.AUTO)
    private Integer id;
    private String type;
    private String name;
    private String description;

}
