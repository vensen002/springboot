package com.vensen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.vensen.model.Book;

/**
 * @author by vense
 * @Classname IBookService
 * @Description TODO()
 * @Date 2022/4/14 14:15
 */
public interface IBookService extends IService<Book> {

    IPage<Book> getPage(Integer currentPage, Integer pageSize);

    IPage<Book> getPage(Integer currentPage, Integer pageSize, Book book);
}
