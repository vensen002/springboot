package com.vensen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vensen.model.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author by vense
 * @Classname BookMapper
 * @Description TODO()
 * @Date 2022/4/13 19:29
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {


}
