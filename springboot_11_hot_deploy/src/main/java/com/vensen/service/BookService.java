package com.vensen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.vensen.model.Book;

import java.util.List;

/**
 * @author by vense
 * @Classname BookService
 * @Description TODO()
 * @Date 2022/4/14 9:35
 */
public interface BookService {

    Boolean save(Book book);
    Boolean update(Book book);
    Boolean delete(Integer id);
    Book getById(Integer id);
    List<Book> getAll();

    IPage<Book> getPage(int currentPage, int pageSize);


}
