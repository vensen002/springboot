package com.vensen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vensen.mapper.BookMapper;
import com.vensen.model.Book;
import com.vensen.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author by vense
 * @Classname BookServiceImpl
 * @Description TODO()
 * @Date 2022/4/14 9:36
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper mapper;

    @Override
    public Boolean save(Book book) {
        return mapper.insert(book) > 0;
    }

    @Override
    public Boolean update(Book book) {
        return mapper.updateById(book) > 0;
    }

    @Override
    public Boolean delete(Integer id) {
        return mapper.deleteById(id) > 0;
    }

    @Override
    public Book getById(Integer id) {
        return mapper.selectById(id);
    }

    @Override
    public List<Book> getAll() {
        return mapper.selectList(null);
    }

    @Override
    public IPage<Book> getPage(int currentPage, int pageSize) {
        IPage<Book> page = new Page<>(currentPage, pageSize);
        return mapper.selectPage(page, null);
    }
}
