package com.vensen.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vensen.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;

/**
 * @author by vense
 * @Classname BookMapperTest
 * @Description TODO()
 * @Date 2022/4/14 9:54
 */
@SpringBootTest
public class BookMapperTest {

    @Autowired
    private BookMapper bookMapper;

    @Test
    void test() {
        for (int i = 0; i < 20; i++) {
            Book book = new Book();
            book.setName("测试名称" + i);
            book.setType("测试类型" + i);
            book.setDescription("测试描述" + i);
            bookMapper.insert(book);
        }
    }

    @Test
    void testSelectAll() {
        bookMapper.selectList(null);
    }

    @Test
    void testGetPage() {
        Page page = new Page(2, 5);
        bookMapper.selectPage(page, null);
    }

    @Test
    void testGetBy() {

    }
}
