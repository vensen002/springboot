package com.vensen.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author by vense
 * @Classname ServerConfig
 * @Description TODO()
 * @Date 2022/4/17 8:57
 */
//@Component
@Data
@ConfigurationProperties(prefix = "servers")
public class ServerConfig {

    private String ipAddress;
    private int port;
    private long timeout;
}
