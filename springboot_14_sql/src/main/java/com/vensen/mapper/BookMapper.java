package com.vensen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vensen.model.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author by vense
 * @Classname BookMapper
 * @Description BookMapper
 * @Date 2022/4/18 17:33
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {

}
