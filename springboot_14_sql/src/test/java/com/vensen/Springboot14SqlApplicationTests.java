package com.vensen;

import com.vensen.mapper.BookMapper;
import com.vensen.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Springboot14SqlApplicationTests {

    @Autowired
    private BookMapper mapper;

    @Test
    void contextLoads() {
        mapper.selectById(1);

    }

    @Test
    void h2Test() {
        Book book = new Book();
        book.setId(3);
        book.setType("springboot3");
        book.setName("springboot3");
        book.setDescription("springboot3");
        mapper.insert(book);
    }

}
