package com.vensen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot16MongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot16MongoApplication.class, args);
    }

}
