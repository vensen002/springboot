package com.vensen.model;

import lombok.Data;

/**
 * @author by vense
 * @Classname Book
 * @Description TODO()
 * @Date 2022/4/13 19:24
 */
@Data
public class Book {

    private Integer id;
    private String type;
    private String name;
    private String description;

}
