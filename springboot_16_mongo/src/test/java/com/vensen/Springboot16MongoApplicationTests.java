package com.vensen;

import com.vensen.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

@SpringBootTest
class Springboot16MongoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    void contextLoads() {
//        mongoTemplate.find({});
        Book book = new Book();
        book.setId(2);
        book.setName("springboot3");
        book.setType("springboot3");
        book.setDescription("springboot3");
        Book save = mongoTemplate.save(book);
        System.out.println(save);
    }

    @Test
    void findAllTest() {
        List<Book> books = mongoTemplate.findAll(Book.class);
        System.out.println(books);
    }

}
